from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('<str:key>', views.get_content),
]