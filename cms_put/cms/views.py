from django.shortcuts import render
from django.http import HttpResponse
from .models import Content
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
# Para usar put y post en django necesitamos decoradores de datos

@csrf_exempt
def get_content(request, key):
    keys = Content.objects.values_list('key', flat=True)
    if request.method == "PUT":
        value = request.body.decode('utf-8')
        if key in keys:
            content = Content.objects.get(key=key)
            content.value = value
        else:
            content = Content(key=key, value=value)
        content.save()
        keys = Content.objects.values_list('key', flat=True)
    if key in keys:
        content = Content.objects.get(key=key)
        return HttpResponse(content.value)
    else:
        return HttpResponse("Resource not found for key: " + key)
